﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PeregruzkaOperatorov
{


    class Matrix : Exception
    {
        public enum FillType
        {
            Zero,
            Random
        }

        private double[,] matrix;
        private static Random rnd = new Random();

        #region Constructors

        public Matrix(int rows, int cols, FillType fillType)
        {
            matrix = new double[rows, cols];

            switch (fillType)
            {
                case FillType.Zero:
                    ClearMatrix();
                    break;
                case FillType.Random:
                    FillRandomValues(0, 10);
                    break;
            }

        }

        public Matrix(double[,] matrix)
        {
            //this.matrix = matrix;
            this.matrix = new double[matrix.GetLength(0), matrix.GetLength(1)];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    this.matrix[i, j] = matrix[i, j];
                }
            }
        }

        public Matrix(Matrix m)
        {
            this.matrix = new double[m.Rows, m.Cols];

            for (int i = 0; i < m.Rows; i++)
            {
                for (int j = 0; j < m.Cols; j++)
                {
                    this.matrix[i, j] = m.matrix[i, j];
                }
            }
        }

        #endregion

        #region Props

        public int Rows { get { return matrix.GetLength(0); } }
        public int Cols { get { return matrix.GetLength(1); } }

        public double this[int i, int j]
        {
            set { this.SetValue(i, j, value); }
            get { return this.GetValue(i, j); }
        }

        #endregion

        #region SupportMethods

        public void FillRandomValues(int min, int max)
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    matrix[i, j] = rnd.Next(min, max);
                }
            }
        }

        public void ClearMatrix()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    matrix[i, j] = 0;
                }
            }
        }

        public double GetValue(int i, int j)
        {
            if (i < 0 || i > Rows - 1 || j < 0 || j > Cols - 1)
            {
                throw new Exception("Out of range exception");
            }

            return matrix[i, j];
        }

        public void SetValue(int i, int j, double value)
        {
            if (i < 0 || i > Rows - 1 || j < 0 || j > Cols - 1)
            {
                throw new Exception("Out of range exception");
            }

            matrix[i, j] = value;
        }

        #endregion

        #region NormalMethods

        public Matrix Add(Matrix m)
        {
            if (this.Rows == m.Rows && this.Cols == m.Cols)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        this.matrix[i, j] += m.matrix[i, j];
                    }
                }

                return this;
            }
            else
            {
                throw new Exception("Sizes are not equal");
            }
        }

        public Matrix Sub(Matrix m)
        {
            if (this.Rows == m.Rows && this.Cols == m.Cols)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        this.matrix[i, j] -= m.matrix[i, j];
                    }
                }

                return this;
            }
            else
            {
                throw new Exception("Sizes are not equal");
            }
        }

        public int CompareTo(Matrix m)
        {
            if (this.Rows == m.Rows && this.Cols == m.Cols)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        if (this.matrix[i, j] < m.matrix[i, j])
                        {
                            return -1;
                        }
                        else if (this.matrix[i, j] > m.matrix[i, j])
                        {
                            return 1;
                        }
                    }
                }

                return 0;
            }
            else
            {
                throw new Exception("Sizes are not equal");
            }
        }

        #endregion

        #region OverrideMethods

        public override string ToString()
        {
            string output = "";

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    output += matrix[i, j].ToString().PadLeft(4);
                }

                output += "\n";
            }

            return output;
        }

        public override bool Equals(object obj)
        {
            return this.CompareTo((Matrix) obj) == 0;
        }

        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            Matrix mRes = new Matrix(m1);
            mRes.Add(m2);
            return mRes;
        }

        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            Matrix mRes = new Matrix(m1);
            mRes.Sub(m2);
            return mRes;
        }

        public static bool operator ==(Matrix m1, Matrix m2)
        {
            return m1.CompareTo(m2) == 0;
        }

        public static bool operator !=(Matrix m1, Matrix m2)
        {
            return m1.CompareTo(m2) != 0;
        }

        public static bool operator >(Matrix m1, Matrix m2)
        {
            return m1.CompareTo(m2) == 1;
        }

        public static bool operator <(Matrix m1, Matrix m2)
        {
            return m1.CompareTo(m2) == -1;
        }

        public static bool operator >=(Matrix m1, Matrix m2)
        {
            return m1.CompareTo(m2) == 1 || m1.CompareTo(m2) == 0;
        }

        public static bool operator <=(Matrix m1, Matrix m2)
        {
            return m1.CompareTo(m2) == -1 || m1.CompareTo(m2) == 0;
        }

        public static Matrix operator ++(Matrix m)
        {
            for (int i = 0; i < m.Rows; i++)
            {
                for (int j = 0; j < m.Cols; j++)
                {
                    m.matrix[i, j]++;
                }
            }

            return m;
        }

        #endregion
    }
}
